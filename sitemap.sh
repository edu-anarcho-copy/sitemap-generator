#!/bin/bash
#
# Sitemap Generator for edu.anarcho-copy.org
#
# usage: sitemap.sh <output>
#

cd "$(dirname "${BASH_SOURCE[0]}")"
OUTPUT="$1"
[ -z "${OUTPUT}" ] && OUTPUT=$(tty)

SITE="https://edu.anarcho-copy.org"
_get_date() { date -r "${1}" -u +"%Y-%m-%dT%H:%M:%S%:z" ; }
_uri() { echo "${1}" | perl -MURI::file -e 'print URI::file->new(<STDIN>)."\n"' ; }

find ./files/ -type f \( -name "*.pdf" -o -name "*.epub" -o -name "*.PDF" -o -name "*.EPUB" \) > .file_list_1
sed -i 's/\.\/files\//\//g' .file_list_1

find ./files/ -maxdepth 2 -type d -exec echo "{}/" ";" > .dir_list_2
sed -i 's/\.\/files\/\//\//g' .dir_list_2
sed -i 's/\.\/files\//\//g' .dir_list_2

find ./files/ -mindepth 3 -maxdepth 3 -type d -exec echo "{}/" ";" > .dir_list_3
sed -i 's/\.\/files\/\//\//g' .dir_list_3
sed -i 's/\.\/files\//\//g' .dir_list_3

find ./files/ -mindepth 4 -maxdepth 5 -type d -exec echo "{}/" ";" > .dir_list_4
sed -i 's/\.\/files\/\//\//g' .dir_list_4
sed -i 's/\.\/files\//\//g' .dir_list_4

find ./files/ -mindepth 6 -type d -exec echo "{}/" ";" > .dir_list_6
sed -i 's/\.\/files\/\//\//g' .dir_list_6
sed -i 's/\.\/files\//\//g' .dir_list_6

find ./files/  -maxdepth 5 -type f \( -not -name "*.pdf" -and -not -name "*.epub" -and -not -name "*.PDF" -and -not -name "*.EPUB" \) > .file_list_5
sed -i 's/\.\/files\//\//g' .file_list_5


cat > ${OUTPUT} <<EOT
<?xml version="1.0" encoding="UTF-8"?>
<!-- generator="https://gitlab.com/edu-anarcho-copy/sitemap-generator" -->
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
EOT


while read -r FILE; do
URI=$(_uri "${FILE}" | sed 's/file\:\/\///g')
cat >> ${OUTPUT} <<EOT
  <url>
    <loc>${SITE}${URI}</loc>
    <lastmod>$(_get_date "files${FILE}")</lastmod>
    <priority>1.0</priority>
    <changefreq>never</changefreq>
  </url>
EOT
done < .file_list_1


while read -r DIR; do
URI=$(_uri "${DIR}" | sed 's/file\:\/\///g')
cat >> ${OUTPUT} <<EOT
  <url>
    <loc>${SITE}${URI}</loc>
    <lastmod>$(_get_date "files${DIR}")</lastmod>
    <priority>1.0</priority>
    <changefreq>monthly</changefreq>
  </url>
EOT
done < .dir_list_2


while read -r DIR; do
URI=$(_uri "${DIR}" | sed 's/file\:\/\///g')
cat >> ${OUTPUT} <<EOT
  <url>
    <loc>${SITE}${URI}</loc>
    <lastmod>$(_get_date "files${DIR}")</lastmod>
    <priority>0.8</priority>
    <changefreq>monthly</changefreq>
  </url>
EOT
done < .dir_list_3


while read -r DIR; do
URI=$(_uri "${DIR}" | sed 's/file\:\/\///g')
cat >> ${OUTPUT} <<EOT
  <url>
    <loc>${SITE}${URI}</loc>
    <lastmod>$(_get_date "files${DIR}")</lastmod>
    <priority>0.1</priority>
  </url>
EOT
done < .dir_list_4

while read -r DIR; do
URI=$(_uri "${DIR}" | sed 's/file\:\/\///g')
cat >> ${OUTPUT} <<EOT
  <url>
    <loc>${SITE}${URI}</loc>
    <lastmod>$(_get_date "files${DIR}")</lastmod>
    <priority>0.0</priority>
  </url>
EOT
done < .dir_list_6

while read -r FILE; do
URI=$(_uri "${FILE}" | sed 's/file\:\/\///g')
cat >> ${OUTPUT} <<EOT
  <url>
    <loc>${SITE}${URI}</loc>
    <lastmod>$(_get_date "files${FILE}")</lastmod>
    <priority>0.1</priority>
    <changefreq>never</changefreq>
  </url>
EOT
done < .file_list_5


cat >> ${OUTPUT} <<EOT
</urlset>
EOT

rpl "&" "&amp;" ${OUTPUT}
rm -f .dir_list_* .file_list_* &>/dev/null
